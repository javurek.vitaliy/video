import Video from '../video';
var assert = require('assert');
var expect = require('chai').expect;

import 'mocha';
const videoElement = document.createElement('video');
const videoModel = new Video(videoElement);

describe('Add listeners', () => {
    require('jsdom-global')()
    it('Should have listeners', () => {
        (videoModel as any).addListeners(videoElement, [], []);
        expect(videoElement.ontimeupdate).to.be.an('function');
        expect(videoElement.onended).to.be.an('function');
    });
});


describe('Get Hooks', () => {
    it('Should return array of functions', (done: any) => {
        const marker = '3s';
        const resolvingPromise = (videoModel as any).getHooks([marker]);
        resolvingPromise.then((result) => {
            console.log(typeof result[marker]);
            expect(result[marker]).to.be.an('function')
            done();
        }).catch(done);
    });
});

describe('Get Hook Method', () => {
    it('The function should call only once', () => {
        let number = 1;
        const fun = (videoModel as any).getHookMethod(() => number++);
        fun();
        expect(number).to.equal(2);
        fun();
        expect(number).to.equal(2);
    });
});
