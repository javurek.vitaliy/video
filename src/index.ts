import Video from './video';

document.addEventListener("DOMContentLoaded", () => {
    const video = document.getElementById('video');
    const videoModel = new Video((video as HTMLVideoElement), true);

    videoModel.addHooks(
        [{ time: '2s', function: () => console.log('Loging from index.ts. Wached 2 seconds') },
            '25%',
            '50%',
            '75%',
            '100%']
    );
    document.addEventListener('scroll', () => {
        videoModel.isVisible(video) ? videoModel.play() : videoModel.pause();
    });
    videoModel.isVisible(video) ? videoModel.play() : videoModel.pause();
});