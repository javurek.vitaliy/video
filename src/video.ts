import { TimeHook } from './interfaces';

class Video {
    private video: HTMLVideoElement;
    private loop: boolean;
    constructor(video: HTMLVideoElement, loop: boolean = false) {
        this.video = video;
        this.loop = loop;
        this.video.addEventListener('click', () => {
            (video as any).muted = !(video as any).muted;
        })
    }

    public play() {
        this.video.play();
    }

    public pause() {
        this.video.pause();
    }

    private addListeners(video: HTMLVideoElement, functions: any, markers: any): void {
        video.ontimeupdate = this.handleTimeUpdate.bind(this, functions);
        video.onended = () => {
            functions[`100%`] && functions[`100%`]();
            this.loop && this.play();
            this.addHooks(markers);
        }
    }

    private handleTimeUpdate(functions: any): void {
        const seen = (this.video.currentTime * 100) / this.video.duration;
        functions[`${Math.round(this.video.currentTime)}s`] && functions[`${Math.round(this.video.currentTime)}s`]();
        functions[`${Math.round(seen)}%`] && functions[`${Math.round(seen)}%`]();
    }

    public addHooks(markers: Array<string | TimeHook>): void {
        this.getHooks(markers)
            .then((functionsObj) => {
                this.addListeners(this.video, functionsObj, markers);
            })
            .catch(e => console.error(e))
    }

    private getHooks(markers: Array<string | TimeHook>): Promise<any[]> {

        return new Promise((resolve, reject) => {
            let functionsObj: any = {};
            if (markers.length) {
                for (const marker of markers) {
                    if (typeof marker === 'object' && marker.time && marker.function) {
                        functionsObj[marker.time] = this.getHookMethod(marker.function);
                    } else if (typeof marker === 'string') {
                        functionsObj[marker] = this.getHookMethod(() => {
                            console.log(`Watched ${marker} in total`);
                        })
                    }
                }
                resolve(functionsObj);
            } else {
                reject('Array markers is empty')
            }
        })

    }

    private getHookMethod(fn: any): Function {
        let result: any;
        return function () {
            if (fn) {
                result = fn.apply();
                fn = null;
            }
            return result;
        };
    }

    public isVisible(element: HTMLElement): boolean {
        const position: any = element.getBoundingClientRect();
        return window.innerHeight - position.y >= element.clientHeight / 2 &&
            window.innerHeight - position.y <= element.clientHeight / 2 + window.innerHeight
    }
}

export default Video;